# Pros and Cons

Simple static web application that helps to make a decision based on it advantages and disadvantages. It has simple design for simplicity purpose (and because I'm not a designer too 😢).

## Stage of development

The project is in early development stage, so now there are no user-ready example. At this moment.

## Prototype

![Prototype screenshot](static/prototype.png)


<div>Icons made by <a href="https://www.flaticon.com/authors/pixelmeetup" title="Pixelmeetup">Pixelmeetup</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
