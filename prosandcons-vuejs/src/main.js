import Vue from 'vue';
import App from './App.vue';

import TextareaAutosize from 'vue-textarea-autosize'

Vue.use(TextareaAutosize)
new Vue({
  el: '#app',
  render: h => h(App),
});